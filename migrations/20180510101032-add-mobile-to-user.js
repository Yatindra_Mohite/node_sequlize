'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
     queryInterface.addColumn( 'Users', 'mobile', Sequelize.STRING );
  },

  down: (queryInterface, Sequelize) => {
      queryInterface.removeColumn( 'Users', 'mobile' );
  }
};
