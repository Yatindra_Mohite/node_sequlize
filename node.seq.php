NODE JS WITH ORM SEQULIZE

//Create and initialize your a directory for your Express application.
$ mkdir sequelize-demo 
$ cd sequelize-demo
$ npm init

//After you are inside the project directory, install sequelize-cli globally.
$ npm install sequelize-cli -g

//This will allow us to use the sequelize is a command line tool that helps you create and manage your sequelize files.

//In addition, you will need to also install sequelize module localy in order to utilize the command line tool.

npm install sequelize --save

//Let's start by creating a our configuration file using:
$ sequelize init

//Configuring your database
sudo npm install mysql --save

//Change database connection credentials in following file
/config/config.json

//Creating Models and migrations
sequelize model:create --name User --attributes first_name:string,last_name:string,bio:text

//When you want to add one or more column after create model then use this
sequelize migration:create --name add-email-to-user